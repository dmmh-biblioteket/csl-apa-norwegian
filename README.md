# A Citation Style Language (CSL) implementation of APA 6th for the Norwegian Language

(**tl:dr**: If you would like to use this file with Zotero, you should simply download the `csl-apa-norwegian.xml` file and import it into the *Style Manager* in Zotero.)

The aim of this project is to implement the standard APA 6th Norwegian style in a CSL file. The primary benefit of doing this is the use of the file with the open source reference managemenent program [Zotero](https://www.zotero.org/).
